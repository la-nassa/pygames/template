# la nassa pygame's template

A super basic template for a pygame game.

Just git clone this repo and start developing your game.

You already get a start screen with a small menu, a game play and a game over screen. Just focus on writing the game!

To clone the project into a new folder (your game will have a different name than template I guess), just run

```
git clone git@gitlab.com:la-nassa/pygames/template.git new_name
```
