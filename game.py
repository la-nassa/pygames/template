import pygame
import random
from game_constants import *
from game_texts import *
from game_elements import *

if __name__ == "__main__":

    # pre-initialize the mixer
    # it allows us to prepare the sounds to be played during the game
    pygame.mixer.init(frequency=44100, # standard
                      size=-16, #  standard
                      channels=2, # standard (can choose 1 or 2)
                      buffer=512) # this controls the quality, higher is better
    # initialize the pygame module
    # to be able to use it to render
    pygame.init()
    # choose the font for the game
    # to write something on the screen
    menu_font = pygame.font.Font(None, MENU_FONT_SIZE)
    # initialize the clock
    clock = pygame.time.Clock()

    # graphical stuff
    bg = pygame.image.load("assets/bg/background_lanassa.png")
    playing_bg = pygame.image.load("assets/bg/game_bg.jpg")

    # this will be set to False to quit the game
    game_is_running = True
    # different screens
    main_menu_active = True # we start from here
    playing = False # we get here when we start
    game_over = False # we get here from playing
    instructions_menu_active = False # explanations
    options_menu_active = False # options: audio, video, ecc..
    credits_menu_active = False # credits: who made the game



    ### SCREEN
    screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
    # set the pygame window name
    pygame.display.set_caption("pygame template - lanassa")

    # IMPORTANT:
    # first set video mode (here above)
    # and then create objects with images
    # (the player for example would raise error
    # if created before setting video mode)

    # instantiate the objects
    main_menu_sprite = MainMenu(menu_font, bg)
    instr_menu_sprite = InstructionsMenu(menu_font, bg)
    credits_menu_sprite = CreditsMenu(menu_font, bg)
    game_over_sprite = GameOverMenu(menu_font, bg)

    # our player!
    player = Player()

    while game_is_running:

        pressed_keys = pygame.key.get_pressed()

        ## MAIN MENU SCREEN
        if main_menu_active is True:
            action = main_menu_sprite.update(pressed_keys)
            if not action:
                main_menu_sprite.draw(screen)
            # action can be:
            # "start", "options", "instructions", "credits", "quit"
            # defined in game_constants as "CHOICES"
            # probably not the best
            elif action == "start":
                main_menu_active = False
                playing = True
            elif action == "options":
                main_menu_active = True
            elif action == "instructions":
                main_menu_active = False
                instructions_menu_active = True
            elif action == "credits":
                main_menu_active = False
                credits_menu_active = True
            elif action == "quit":
                game_is_running = False

        if instructions_menu_active is True:
            instr_menu_sprite.draw(screen)
            if pressed_keys[K_ESCAPE]:
                instructions_menu_active = False
                main_menu_active = True

        if credits_menu_active is True:
            credits_menu_sprite.draw(screen)
            if pressed_keys[K_ESCAPE]:
                credits_menu_active = False
                main_menu_active = True

        if playing is True:
            # now we are playing
            screen.blit(playing_bg, (0,0))
            screen.blit(player.surf, player.rect)
            touched = player.move(pressed_keys)
            if pressed_keys[K_ESCAPE]:
                playing = False
                main_menu_active = True
            if touched:
                playing = False
                game_over = True

        if game_over is True:
            game_over_sprite.draw(screen)
            if pressed_keys[K_ESCAPE]:
                game_over = False
                main_menu_active = True

        # here all the events
        # (window, focus, mouse, keys)
        # if we do not loop this, they get stuck
        # we DO NOT have to use them each iteration
        # but we can if needed
        for event in pygame.event.get():
            # you can print them to take a look if you want
            # print("event:", event)
            # QUIT
            if event.type == pygame.QUIT:
                game_is_running = False

        pygame.display.flip()
        # Ensure program maintains a rate of X frames per second
        clock.tick(FPS)
