# for the paths
import os
# objects colors
from pygame import Color
# keys
from pygame.locals import (
    RLEACCEL,
    K_UP,
    K_DOWN,
    K_LEFT,
    K_RIGHT,
    K_ESCAPE,
    K_RETURN,
    KEYDOWN,
    QUIT,
)


# CONSTANTS for the game

# resolution
SCREEN_WIDTH = 1080
SCREEN_HEIGHT = 720
# frames per second
FPS = 60
# COLORS
MENU_BACKGROUND_COLOR = (255, 0, 0)
GAME_BACKGROUND_COLOR = (255, 255, 0)
G_OVER_BACKGROUND_COLOR = (127, 127, 127)
# MAIN_MENUS
MENU_START_Y = int(SCREEN_HEIGHT * .4)
MENU_START_X = int(SCREEN_WIDTH * .4)
MENU_TEXTS_STEP = 50
MENU_FONT_SIZE = 35
MENU_KEY_FRAMES_ACCEPTED = FPS / 12
# instructions
INSTR_START_Y = MENU_START_Y
INSTR_START_X = int(SCREEN_WIDTH * .2)
INSTR_STEP = 35
# player
PL_SIZE = 100
PL_MOVE = int(PL_SIZE / 5)
