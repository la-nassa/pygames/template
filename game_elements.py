import pygame
import random
import os
from game_constants import *
from game_texts import *
import numpy as np
import pdb

# images
IMG_FOLDER = "assets/img"

# Define a Player object by extending pygame.sprite.Sprite
# The surface drawn on the screen is now an attribute of 'player'
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super(Player, self).__init__()
        self.surf = pygame.image.load(os.path.join(IMG_FOLDER, "pygame_nassa_white_100.png")).convert_alpha()
        self.rect = self.surf.get_rect(
            center=(
                SCREEN_WIDTH / 2,
                SCREEN_HEIGHT / 2,
            )
        )

    def move(self, pressed_keys):
        if pressed_keys[K_DOWN]:
            self.rect.move_ip(0, +PL_MOVE)
        if pressed_keys[K_UP]:
            self.rect.move_ip(0, -PL_MOVE)
        if pressed_keys[K_LEFT]:
            self.rect.move_ip(-PL_MOVE, 0)
        if pressed_keys[K_RIGHT]:
            self.rect.move_ip(+PL_MOVE, 0)

        # we check if the player touch the border
        # for a normal gameplay, you may want
        # to remove the touched and just keep
        # the player within the screen
        touched = False
        # Keep player on the screen
        if self.rect.left < 0:
            self.rect.left = 0
            touched = True
        if self.rect.right > SCREEN_WIDTH:
            self.rect.right = SCREEN_WIDTH
            touched = True
        if self.rect.top <= 0:
            self.rect.top = 0
            touched = True
        if self.rect.bottom >= SCREEN_HEIGHT:
            self.rect.bottom = SCREEN_HEIGHT
            touched = True

        return touched

class MainMenu(pygame.sprite.Sprite):
    def __init__(self, font, bg):
        super(MainMenu, self).__init__()
        self.current_choice = 0
        self.key_counter = 0
        self.last_key = K_RETURN
        self.key_accept = MENU_KEY_FRAMES_ACCEPTED
        self.bg = bg
        self.texts = []
        for i in range(len(MENU_OPTIONS)):
            self.texts.append(font.render(f"{MENU_OPTIONS[i]}", True, (255, 255, 255)))

    def update(self, pressed_keys):
        action = None;
        if pressed_keys[K_DOWN]:
            if self.last_key == K_DOWN:
                self.action_counter += 1
            else:
                self.last_key = K_DOWN
                self.action_counter = 1
            if self.action_counter >= self.key_accept:
                self.current_choice = min(self.current_choice + 1, len(MENU_OPTIONS)-1)
                self.action_counter = 0
        if pressed_keys[K_UP]:
            if self.last_key == K_UP:
                self.action_counter += 1
            else:
                self.last_key = K_UP
                self.action_counter = 1
            if self.action_counter >= self.key_accept:
                self.current_choice = max(self.current_choice - 1, 0)
                self.action_counter = 0
        if pressed_keys[K_RETURN]:
            action = MENU_OPTIONS[self.current_choice]
        #if pressed_keys[K_ESCAPE]:
        #    action = "quit"

        return action

    def draw(self, screen):
        screen.blit(self.bg, (0,0))
        for i, text in enumerate(self.texts):
            screen.blit(text, (MENU_START_X, MENU_START_Y + i * MENU_TEXTS_STEP))
        pygame.draw.circle(screen, (255, 255, 255), (MENU_START_X - 50, MENU_START_Y + self.current_choice * MENU_TEXTS_STEP), 15, 1)

class InstructionsMenu(pygame.sprite.Sprite):
    def __init__(self, font, bg):
        super(InstructionsMenu, self).__init__()
        self.bg = bg
        self.texts = []
        for i, textline in enumerate(INSTRUCTIONS_TEXT):
            self.texts.append(font.render(f"{textline}", True, (255, 255, 255)))

    def draw(self, screen):
        screen.blit(self.bg, (0,0))
        for i, text_obj in enumerate(self.texts):
            screen.blit(text_obj, (INSTR_START_X, INSTR_START_Y + i * INSTR_STEP))

class CreditsMenu(pygame.sprite.Sprite):
    def __init__(self, font, bg):
        super(CreditsMenu, self).__init__()
        self.bg = bg
        self.texts = []
        for i, textline in enumerate(CREDITS_TEXT):
            self.texts.append(font.render(f"{textline}", True, (255, 255, 255)))

    def draw(self, screen):
        screen.blit(self.bg, (0,0))
        for i, text_obj in enumerate(self.texts):
            screen.blit(text_obj, (INSTR_START_X, INSTR_START_Y + i * INSTR_STEP))

class GameOverMenu(pygame.sprite.Sprite):
    def __init__(self, font, bg):
        super(GameOverMenu, self).__init__()
        self.bg = bg
        self.texts = []
        for i, textline in enumerate(GAME_OVER_TEXT):
            self.texts.append(font.render(f"{textline}", True, (255, 255, 255)))

    def draw(self, screen):
        screen.blit(self.bg, (0,0))
        for i, text_obj in enumerate(self.texts):
            screen.blit(text_obj, (INSTR_START_X, INSTR_START_Y + i * INSTR_STEP))
