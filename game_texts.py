MENU_OPTIONS = ["start", "options", "instructions", "credits", "quit"]
INSTRUCTIONS_TEXT = ["This is a template made with pygame",
                     "edit this project to create a new game",
                     "and release it as open source software",
                     "", "", "",
                     "PRESS ESC TO RETURN BACK TO MAIN MENU"]
CREDITS_TEXT = ["Thanks to:",
                "Pygame -- pygame.org",
                "la nassa -- lanassa.net",
                "FelixMittermeier for the image -- https://pixabay.com/users/felixmittermeier-4397258/"
                "", "", "",
                "PRESS ESC TO RETURN BACK TO MAIN MENU"]
GAME_OVER_TEXT = ["GAME OVER",
                  "here you could have text when you lose.",
                  "", "", "",
                  "PRESS ESC TO RETURN BACK TO MAIN MENU"]
